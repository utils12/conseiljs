"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function id(d) { return d[0]; }
const moo = require("moo");
const bigInt = require("big-integer");
const MichelineKeywords = ['"parameter"', '"storage"', '"code"', '"False"', '"Elt"', '"Left"', '"None"', '"Pair"', '"Right"', '"Some"', '"True"', '"Unit"', '"PACK"', '"UNPACK"', '"BLAKE2B"', '"SHA256"', '"SHA512"', '"ABS"', '"ADD"', '"AMOUNT"', '"AND"', '"BALANCE"', '"CAR"', '"CDR"', '"CHECK_SIGNATURE"', '"COMPARE"', '"CONCAT"', '"CONS"', '"CREATE_ACCOUNT"', '"CREATE_CONTRACT"', '"IMPLICIT_ACCOUNT"', '"DIP"', '"DROP"', '"DUP"', '"EDIV"', '"EMPTY_MAP"', '"EMPTY_SET"', '"EQ"', '"EXEC"', '"FAILWITH"', '"GE"', '"GET"', '"GT"', '"HASH_KEY"', '"IF"', '"IF_CONS"', '"IF_LEFT"', '"IF_NONE"', '"INT"', '"LAMBDA"', '"LE"', '"LEFT"', '"LOOP"', '"LSL"', '"LSR"', '"LT"', '"MAP"', '"MEM"', '"MUL"', '"NEG"', '"NEQ"', '"NIL"', '"NONE"', '"NOT"', '"NOW"', '"OR"', '"PAIR"', '"PUSH"', '"RIGHT"', '"SIZE"', '"SOME"', '"SOURCE"', '"SENDER"', '"SELF"', '"STEPS_TO_QUOTA"', '"SUB"', '"SWAP"', '"TRANSFER_TOKENS"', '"SET_DELEGATE"', '"UNIT"', '"UPDATE"', '"XOR"', '"ITER"', '"LOOP_LEFT"', '"ADDRESS"', '"CONTRACT"', '"ISNAT"', '"CAST"', '"RENAME"', '"bool"', '"contract"', '"int"', '"key"', '"key_hash"', '"lambda"', '"list"', '"map"', '"big_map"', '"nat"', '"option"', '"or"', '"pair"', '"set"', '"signature"', '"string"', '"bytes"', '"mutez"', '"timestamp"', '"unit"', '"operation"', '"address"', '"SLICE"', '"DIG"', '"DUG"', '"EMPTY_BIG_MAP"', '"APPLY"', '"chain_id"', '"CHAIN_ID"'];
const lexer = moo.compile({
    keyword: MichelineKeywords,
    lbrace: '{',
    rbrace: '}',
    lbracket: '[',
    rbracket: ']',
    colon: ":",
    comma: ",",
    _: /[ \t]+/,
    quotedValue: /\"[\S\s]*?\"/
});
const staticIntToHex = d => {
    const prefix = '00';
    const text = d[6].toString();
    const value = writeSignedInt(parseInt(text.substring(1, text.length - 1)));
    return prefix + value;
};
const staticStringToHex = d => {
    const prefix = '01';
    let text = d[6].toString();
    text = text.substring(1, text.length - 1);
    const len = encodeLength(text.length);
    text = text.split('').map(c => c.charCodeAt(0).toString(16)).join('');
    return prefix + len + text;
};
const staticBytesToHex = d => {
    const prefix = '0a';
    let bytes = d[6].toString();
    bytes = bytes.substring(1, bytes.length - 1);
    const len = encodeLength(bytes.length / 2);
    return prefix + len + bytes;
};
const staticArrayToHex = d => {
    const matchedArray = d[2];
    const prefix = '02';
    const content = matchedArray.map(a => a[0]).join('');
    const len = encodeLength(content.length / 2);
    return prefix + len + content;
};
const primBareToHex = d => {
    const prefix = '03';
    const prim = encodePrimitive(d[6].toString());
    return prefix + prim;
};
const primAnnToHex = d => {
    const prefix = '04';
    const prim = encodePrimitive(d[6].toString());
    let ann = d[15].map(v => {
        let t = v[0].toString();
        t = t.substring(1, t.length - 1);
        return t;
    }).join(' ');
    ann = ann.split('').map(c => c.charCodeAt(0).toString(16)).join('');
    ann = encodeLength(ann.length / 2) + ann;
    return prefix + prim + ann;
};
const primArgToHex = d => {
    let prefix = '05';
    if (d[15].length == 2) {
        prefix = '07';
    }
    else if (d[15].length > 2) {
        prefix = '09';
    }
    const prim = encodePrimitive(d[6].toString());
    let args = d[15].map(v => v[0]).join('');
    if (prefix === '09') {
        args = ('0000000' + (args.length / 2).toString(16)).slice(-8) + args;
        args += '00000000';
    }
    return prefix + prim + args;
};
const primArgAnnToHex = d => {
    let prefix = '06';
    if (d[15].length == 2) {
        prefix = '08';
    }
    else if (d[15].length > 2) {
        prefix = '09';
    }
    const prim = encodePrimitive(d[6].toString());
    let args = d[15].map(v => v[0]).join('');
    let ann = d[26].map(v => {
        let t = v[0].toString();
        t = t.substring(1, t.length - 1);
        return t;
    }).join(' ');
    ann = ann.split('').map(c => c.charCodeAt(0).toString(16)).join('');
    ann = encodeLength(ann.length / 2) + ann;
    if (prefix === '09') {
        args = ('0000000' + (args.length / 2).toString(16)).slice(-8) + args;
    }
    return prefix + prim + args + ann;
};
const encodePrimitive = p => {
    return ('00' + MichelineKeywords.indexOf(p).toString(16)).slice(-2);
};
const encodeLength = l => {
    return ('0000000' + l.toString(16)).slice(-8);
};
const writeSignedInt = value => {
    if (value === 0) {
        return '00';
    }
    const n = bigInt(value).abs();
    const l = n.bitLength().toJSNumber();
    let arr = [];
    let v = n;
    for (let i = 0; i < l; i += 7) {
        let byte = bigInt.zero;
        if (i === 0) {
            byte = v.and(0x3f);
            v = v.shiftRight(6);
        }
        else {
            byte = v.and(0x7f);
            v = v.shiftRight(7);
        }
        if (value < 0 && i === 0) {
            byte = byte.or(0x40);
        }
        if (i + 7 < l) {
            byte = byte.or(0x80);
        }
        arr.push(byte.toJSNumber());
    }
    if (l % 7 === 0) {
        arr[arr.length - 1] = arr[arr.length - 1] | 0x80;
        arr.push(1);
    }
    return arr.map(v => ('0' + v.toString(16)).slice(-2)).join('');
};
;
;
;
;
const grammar = {
    Lexer: lexer,
    ParserRules: [
        { "name": "main", "symbols": ["staticObject"], "postprocess": id },
        { "name": "main", "symbols": ["primBare"], "postprocess": id },
        { "name": "main", "symbols": ["primArg"], "postprocess": id },
        { "name": "main", "symbols": ["primAnn"], "postprocess": id },
        { "name": "main", "symbols": ["primArgAnn"], "postprocess": id },
        { "name": "main", "symbols": ["anyArray"], "postprocess": id },
        { "name": "staticInt$ebnf$1", "symbols": [] },
        { "name": "staticInt$ebnf$1", "symbols": ["staticInt$ebnf$1", (lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "staticInt", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"int\"" }, "staticInt$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": staticIntToHex },
        { "name": "staticString$ebnf$1", "symbols": [] },
        { "name": "staticString$ebnf$1", "symbols": ["staticString$ebnf$1", (lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "staticString", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"string\"" }, "staticString$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": staticStringToHex },
        { "name": "staticBytes$ebnf$1", "symbols": [] },
        { "name": "staticBytes$ebnf$1", "symbols": ["staticBytes$ebnf$1", (lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "staticBytes", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"bytes\"" }, "staticBytes$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": staticBytesToHex },
        { "name": "staticObject", "symbols": ["staticInt"], "postprocess": id },
        { "name": "staticObject", "symbols": ["staticString"], "postprocess": id },
        { "name": "staticObject", "symbols": ["staticBytes"], "postprocess": id },
        { "name": "primBare$ebnf$1", "symbols": [] },
        { "name": "primBare$ebnf$1", "symbols": ["primBare$ebnf$1", (lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "primBare", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"prim\"" }, "primBare$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("keyword") ? { type: "keyword" } :MichelineKeywords), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": primBareToHex },
        { "name": "primArg$ebnf$1", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArg$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArg$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArg$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArg$ebnf$3$subexpression$1$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primArg$ebnf$3$subexpression$1$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArg$ebnf$3$subexpression$1$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArg$ebnf$3$subexpression$1$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArg$ebnf$3$subexpression$1", "symbols": ["any", "primArg$ebnf$3$subexpression$1$ebnf$1", "primArg$ebnf$3$subexpression$1$ebnf$2"] },
        { "name": "primArg$ebnf$3", "symbols": ["primArg$ebnf$3$subexpression$1"] },
        { "name": "primArg$ebnf$3$subexpression$2$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primArg$ebnf$3$subexpression$2$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArg$ebnf$3$subexpression$2$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArg$ebnf$3$subexpression$2$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArg$ebnf$3$subexpression$2", "symbols": ["any", "primArg$ebnf$3$subexpression$2$ebnf$1", "primArg$ebnf$3$subexpression$2$ebnf$2"] },
        { "name": "primArg$ebnf$3", "symbols": ["primArg$ebnf$3", "primArg$ebnf$3$subexpression$2"], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "primArg", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"prim\"" }, "primArg$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("keyword") ? { type: "keyword" } :MichelineKeywords), (lexer.has("comma") ? { type: "comma" } : ","), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"args\"" }, "primArg$ebnf$2", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("lbracket") ? { type: "lbracket" } : '['), (lexer.has("_") ? { type: "_" } : /[ \t]+/), "primArg$ebnf$3", (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbracket") ? { type: "rbracket" } : ']'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": primArgToHex },
        { "name": "primAnn$ebnf$1", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primAnn$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primAnn$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primAnn$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primAnn$ebnf$3$subexpression$1$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primAnn$ebnf$3$subexpression$1$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primAnn$ebnf$3$subexpression$1$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primAnn$ebnf$3$subexpression$1$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primAnn$ebnf$3$subexpression$1", "symbols": [(lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), "primAnn$ebnf$3$subexpression$1$ebnf$1", "primAnn$ebnf$3$subexpression$1$ebnf$2"] },
        { "name": "primAnn$ebnf$3", "symbols": ["primAnn$ebnf$3$subexpression$1"] },
        { "name": "primAnn$ebnf$3$subexpression$2$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primAnn$ebnf$3$subexpression$2$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primAnn$ebnf$3$subexpression$2$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primAnn$ebnf$3$subexpression$2$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primAnn$ebnf$3$subexpression$2", "symbols": [(lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), "primAnn$ebnf$3$subexpression$2$ebnf$1", "primAnn$ebnf$3$subexpression$2$ebnf$2"] },
        { "name": "primAnn$ebnf$3", "symbols": ["primAnn$ebnf$3", "primAnn$ebnf$3$subexpression$2"], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "primAnn", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"prim\"" }, "primAnn$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("keyword") ? { type: "keyword" } :MichelineKeywords), (lexer.has("comma") ? { type: "comma" } : ","), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"annots\"" }, "primAnn$ebnf$2", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("lbracket") ? { type: "lbracket" } : '['), (lexer.has("_") ? { type: "_" } : /[ \t]+/), "primAnn$ebnf$3", (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbracket") ? { type: "rbracket" } : ']'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": primAnnToHex },
        { "name": "primArgAnn$ebnf$1", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$3$subexpression$1$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primArgAnn$ebnf$3$subexpression$1$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$3$subexpression$1$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$3$subexpression$1$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$3$subexpression$1", "symbols": ["any", "primArgAnn$ebnf$3$subexpression$1$ebnf$1", "primArgAnn$ebnf$3$subexpression$1$ebnf$2"] },
        { "name": "primArgAnn$ebnf$3", "symbols": ["primArgAnn$ebnf$3$subexpression$1"] },
        { "name": "primArgAnn$ebnf$3$subexpression$2$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primArgAnn$ebnf$3$subexpression$2$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$3$subexpression$2$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$3$subexpression$2$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$3$subexpression$2", "symbols": ["any", "primArgAnn$ebnf$3$subexpression$2$ebnf$1", "primArgAnn$ebnf$3$subexpression$2$ebnf$2"] },
        { "name": "primArgAnn$ebnf$3", "symbols": ["primArgAnn$ebnf$3", "primArgAnn$ebnf$3$subexpression$2"], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "primArgAnn$ebnf$4", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$4", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$5$subexpression$1$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primArgAnn$ebnf$5$subexpression$1$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$5$subexpression$1$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$5$subexpression$1$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$5$subexpression$1", "symbols": [(lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), "primArgAnn$ebnf$5$subexpression$1$ebnf$1", "primArgAnn$ebnf$5$subexpression$1$ebnf$2"] },
        { "name": "primArgAnn$ebnf$5", "symbols": ["primArgAnn$ebnf$5$subexpression$1"] },
        { "name": "primArgAnn$ebnf$5$subexpression$2$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "primArgAnn$ebnf$5$subexpression$2$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$5$subexpression$2$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "primArgAnn$ebnf$5$subexpression$2$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "primArgAnn$ebnf$5$subexpression$2", "symbols": [(lexer.has("quotedValue") ? { type: "quotedValue" } : /\"[\S\s]*?\"/), "primArgAnn$ebnf$5$subexpression$2$ebnf$1", "primArgAnn$ebnf$5$subexpression$2$ebnf$2"] },
        { "name": "primArgAnn$ebnf$5", "symbols": ["primArgAnn$ebnf$5", "primArgAnn$ebnf$5$subexpression$2"], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "primArgAnn", "symbols": [(lexer.has("lbrace") ? { type: "lbrace" } : '{'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"prim\"" }, "primArgAnn$ebnf$1", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("keyword") ? { type: "keyword" } :MichelineKeywords), (lexer.has("comma") ? { type: "comma" } : ","), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"args\"" }, "primArgAnn$ebnf$2", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("lbracket") ? { type: "lbracket" } : '['), (lexer.has("_") ? { type: "_" } : /[ \t]+/), "primArgAnn$ebnf$3", (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbracket") ? { type: "rbracket" } : ']'), (lexer.has("comma") ? { type: "comma" } : ","), (lexer.has("_") ? { type: "_" } : /[ \t]+/), { "literal": "\"annots\"" }, "primArgAnn$ebnf$4", (lexer.has("colon") ? { type: "colon" } : ":"), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("lbracket") ? { type: "lbracket" } : '['), (lexer.has("_") ? { type: "_" } : /[ \t]+/), "primArgAnn$ebnf$5", (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbracket") ? { type: "rbracket" } : ']'), (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbrace") ? { type: "rbrace" } :'}')], "postprocess": primArgAnnToHex },
        { "name": "primAny", "symbols": ["primBare"], "postprocess": id },
        { "name": "primAny", "symbols": ["primArg"], "postprocess": id },
        { "name": "primAny", "symbols": ["primAnn"], "postprocess": id },
        { "name": "primAny", "symbols": ["primArgAnn"], "postprocess": id },
        { "name": "any", "symbols": ["primAny"], "postprocess": id },
        { "name": "any", "symbols": ["staticObject"], "postprocess": id },
        { "name": "any", "symbols": ["anyArray"], "postprocess": id },
        { "name": "anyArray", "symbols": [(lexer.has("lbracket") ? { type: "lbracket" } : '['), (lexer.has("rbracket") ? { type: "rbracket" } : ']')], "postprocess": function (d) { return '0200000000'; } },
        { "name": "anyArray$ebnf$1$subexpression$1$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "anyArray$ebnf$1$subexpression$1$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "anyArray$ebnf$1$subexpression$1$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "anyArray$ebnf$1$subexpression$1$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "anyArray$ebnf$1$subexpression$1", "symbols": ["any", "anyArray$ebnf$1$subexpression$1$ebnf$1", "anyArray$ebnf$1$subexpression$1$ebnf$2"] },
        { "name": "anyArray$ebnf$1", "symbols": ["anyArray$ebnf$1$subexpression$1"] },
        { "name": "anyArray$ebnf$1$subexpression$2$ebnf$1", "symbols": [(lexer.has("comma") ? { type: "comma" } : ",")], "postprocess": id },
        { "name": "anyArray$ebnf$1$subexpression$2$ebnf$1", "symbols": [], "postprocess": () => null },
        { "name": "anyArray$ebnf$1$subexpression$2$ebnf$2", "symbols": [(lexer.has("_") ? { type: "_" } : /[ \t]+/)], "postprocess": id },
        { "name": "anyArray$ebnf$1$subexpression$2$ebnf$2", "symbols": [], "postprocess": () => null },
        { "name": "anyArray$ebnf$1$subexpression$2", "symbols": ["any", "anyArray$ebnf$1$subexpression$2$ebnf$1", "anyArray$ebnf$1$subexpression$2$ebnf$2"] },
        { "name": "anyArray$ebnf$1", "symbols": ["anyArray$ebnf$1", "anyArray$ebnf$1$subexpression$2"], "postprocess": (d) => d[0].concat([d[1]]) },
        { "name": "anyArray", "symbols": [(lexer.has("lbracket") ? { type: "lbracket" } : '['), (lexer.has("_") ? { type: "_" } : /[ \t]+/), "anyArray$ebnf$1", (lexer.has("_") ? { type: "_" } : /[ \t]+/), (lexer.has("rbracket") ? { type: "rbracket" } : ']')], "postprocess": staticArrayToHex }
    ],
    ParserStart: "main",
};
exports.default = grammar;
//# sourceMappingURL=Micheline.js.map